import Vue from 'vue'
import VueRouter from 'vue-router'
import People from '../views/People.vue'
import Planets from '../views/Planets.vue'
import Home from '../views/Home.vue'
import Films from '../views/Films.vue'
import Film from '../views/Film.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/people',
    name: 'people',
    component: People
  },
  {
    path: '/planets',
    name: 'planets',
    component: Planets
  },
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/films',
    name: 'films',
    component: Films
  },
  {
    path: '/film/:id',
    name: 'film',
    component : Film

}

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
